import get_data as data
import Person as p
import random as ran

UserList = []
user = p.Person

""" This an instance of the arrays that were created on the getdata class,
 these variables going to contain all the information to create users """
name = data.names
lastname = data.lastname
dates = data.dates

""" 
On this function we received the indexes that were generated on the GetNumber function
and these indexes are used to assemble users
"""
def CreatePerson(numName, numLastname1, numLastname2, numDate):
    Person = user
    Person.name = str(name.__getitem__(numName))
    Person.lastname1 = str(lastname.__getitem__(numLastname1))
    Person.lastname2 = str(lastname.__getitem__(numLastname2))
    Person.birthday = str(dates.__getitem__(numDate))
    User = {"Name":Person.name,"Lastname":Person.lastname1,"Lastname2":Person.lastname2,"Birthday":Person.birthday}
    return UserList.append(User)


""" 
This fuction will generate random nums using the random library, the main goal for this function is to generate
the indexes to select an specific value on the name, lastname, autos and languages arrays,
once the random indexes were generated the are sent to CreatePerson function
"""
def GetNumbers():
     numName = ran.randint(0,name.size)-1
     numLastname1 = ran.randint(0,lastname.size)-1
     numLastname2 = ran.randint(0,lastname.size)-1
     numDate = ran.randint(0,dates.size)-1
     CreatePerson(numName,numLastname1,numLastname2,numDate)