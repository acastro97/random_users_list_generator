"""
On this class we going to import the csv files with the names and lastnames from 2 separates csv
"""

""" We import the pandas library to allow us manipulate csv files and the numpy libary to convert the imported csv to arrays """
import pandas
import numpy

""" csv data imported on variables using pandas"""
getcars = pandas.read_csv ('data_imported/cars.csv')
getdates = pandas.read_csv ('data_imported/dates.csv')
getnames = pandas.read_csv ('../UsersListGenerator/data_imported/names.csv')
getlanguages = pandas.read_csv ('../UsersListGenerator/data_imported/languages.csv')
getlastnames = pandas.read_csv ('data_imported/lastnames.csv')

""" csv variables converted on arrays using numpy"""
autos = numpy.array(getcars)
dates = numpy.array(getdates)
languages = numpy.array(getlanguages)
lastname = numpy.array(getlastnames)
names = numpy.array(getnames)