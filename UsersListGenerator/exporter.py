from person_function import UserList
import json
import  pandas as pan
import  numpy as np

UL = UserList

"""
This dictionary will contains the format options to export the data 
"""
FormatToExport = {'1':'.json','2':'.csv'}

"""
This function will export the generated data into a json format
"""
def ExportAsJson(UL):
    DBName = input('Insert your database name: ')+FormatToExport['1']
    for i in UL:
        tf = open(DBName, 'a')
        json.dump(i, tf)
        tf.write("\n")
        tf.close()


"""This function will export the generated data into a csv format"""
def ExportAsCSV(UL):
    DBName = input('Insert your database name: ')+FormatToExport['2']
    SanitizerArray = []
    for i in UL:
       SName = i['Name'].replace("['","").replace("']","")
       SLastname = i['Lastname'].replace("['","").replace("']","")
       SLastname2 = i['Lastname2'].replace("['","").replace("']","")
       SBirthday = i['Birthday'].replace("['","").replace("']","")
       User = {SName,SLastname,SLastname2,SBirthday}
       SanitizerArray.append(User)
    numpyArray = np.array(SanitizerArray)
    dataFrame = pan.DataFrame({"A":numpyArray})
    dataFrame.to_csv(DBName,index=False, header=False)
    HowExport(UL)

"""On this function we can define on which format export the data or finish the program """
def HowExport(UL):
    verifiermessage = input('How would you like export your database? "1 to json", "2 to csv", "3 to finish": ')
    if (verifiermessage == '1' in FormatToExport) or (verifiermessage == 'json' in FormatToExport['1']):
        ExportAsJson(UL)
    elif (verifiermessage == '2' in FormatToExport) or (verifiermessage == 'csv' in FormatToExport['2']):
        ExportAsCSV(UL)
    elif (verifiermessage == '3' in FormatToExport):
        print("Finished")
